package com.golovin.server.classes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;

public class ListBooksId {

    @JsonDeserialize(as = ArrayList.class, contentAs = Integer.class)
    ArrayList<Integer> booksId;

    public ListBooksId() {
        booksId = new ArrayList<>();
    }

    public ArrayList<Integer> getBooksId() {
        return booksId;
    }

    public void setBooksId(ArrayList<Integer> booksId) {
        this.booksId = booksId;
    }

}
