package com.golovin.server.classes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

public class ListLibraryBooksId {

    @JsonDeserialize(as = List.class, contentAs = Integer.class)
    List<Integer> booksId;

    public ListLibraryBooksId() {
        booksId = new ArrayList<>();
    }

    public List<Integer> getBooksId() {
        return booksId;
    }

    public void setBooksId(List<Integer> booksId) {
        this.booksId = booksId;
    }
}
