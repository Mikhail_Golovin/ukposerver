package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SearchBook {
    @JsonProperty
    private String name;

    @JsonProperty
    private int authorId;

    @JsonProperty
    private String annotation;

    @JsonProperty
    private int genre;

    @JsonProperty
    private int views;

    @JsonProperty
    private int coverId;

    @JsonProperty
    private boolean isDraft;

    @JsonProperty
    private long sort;

    public long getSort() {
        return sort;
    }

    public void setSort(long sort) {
        this.sort = sort;
    }

    public SearchBook() {
    }

    public SearchBook(String name, int authorId, String annotation,
                      int genre, int views, int coverId, boolean isDraft, long sort) {
        this.name = name;
        this.authorId = authorId;
        this.annotation = annotation;
        this.genre = genre;
        this.views = views;
        this.coverId = coverId;
        this.isDraft = isDraft;
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public int getGenre() {
        return genre;
    }

    public void setGenre(int genre) {
        this.genre = genre;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    public boolean getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(boolean draft) {
        isDraft = draft;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
