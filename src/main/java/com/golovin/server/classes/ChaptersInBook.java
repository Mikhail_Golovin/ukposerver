package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ChaptersInBook {

    @JsonProperty
    private int chapterId;

    @JsonProperty
    private String name;

    @JsonProperty
    private int numberInBook;

    public ChaptersInBook() {
    }

    public ChaptersInBook(int chapterId, String name, int numberInBook) {
        this.chapterId = chapterId;
        this.name = name;
        this.numberInBook = numberInBook;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getNumberInBook() {
        return numberInBook;
    }

    public void setNumberInBook(int numberInBook) {
        this.numberInBook = numberInBook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
