package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface ChaptersDaoInterface <T, Id extends Serializable> {
    public void persist(Session session, T entity);

    public void update(Session session, T entity);

    public T findById(Session session, Id id);

    public void delete(Session session, T entity);

    public List<T> findAll(Session session);

    public T findChapterOfBook(Session session, Id id);

    public T findLastChapter(Session session, Id id);

    public T findFirstChapter(Session session, Id id);

}
